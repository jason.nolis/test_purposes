variable "s3_env" {
  description = "s3"
  type        = string

  validation {
    condition     = contains(["developmen", "qa")
    error_message = "The load balancer type can either be 'application' or 'network'. 'gateway' type is not supported."
  }
}