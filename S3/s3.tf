terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  alias = "default"
  region = "ca-central-1"
}

resource "aws_s3_bucket" "bucket" {
bucket = "csio-development-alb-access-logs-test-only"
}
 
resource "aws_s3_bucket_public_access_block" "bucket" {
  count = ( $var.s3_env ? 1:0 )
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
 
resource "aws_s3_bucket_ownership_controls" "bucket" {
  bucket = aws_s3_bucket.bucket.id
  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}