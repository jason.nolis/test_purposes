resource "aws_route53_record" "dev" {

  zone_id = var.zone_id
  name    = "csionet.com"
  type    = "A"
  ttl     = 60
  records = ["172.22.255.77", "172.22.255.251"]
  allow_overwrite = false
}