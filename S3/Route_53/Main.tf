resource "aws_route53_zone" "dev-zone" {
name = "csionet.cometome"
}

resource "aws_route53_record" "dev-record" {

  zone_id = "${aws_route53_zone.dev-zone.zone_id}"
  name    = "dev"
  type    = "A"
  ttl     = 60
  records = ["172.22.255.77", "172.22.255.251"]
  # allow_overwrite = false
}


resource "aws_route53_record" "dev-2-record" {

  zone_id = "${aws_route53_zone.dev-zone.zone_id}"
  name    = "dev-2"
  type    = "A"
  ttl     = 60
  records = ["172.22.255.78", "172.22.255.252"]
  #allow_overwrite = false
}